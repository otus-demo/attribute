﻿namespace Attributes;

[Serializable]
class A
{
    private int C { get; set; }

    void SomeMethod()
    {
        Console.WriteLine("Hello!");
    }
}

interface ITest
{
    static int i;
    int dosometh();

}

static class Static
{
    public static int i;

    public static void Method1() { i++; }
}

internal class A1
{
    //[MyTest]
    public void M1()
    {
        Console.WriteLine("test");
    }

}