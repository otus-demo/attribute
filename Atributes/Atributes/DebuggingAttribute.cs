﻿using System.Diagnostics;

namespace Attributes;

[DebuggerDisplay($"Important debugger information from attribute - {{i}} ")]
[MyTest("Test Attribute",AdditionalProperty = "SomeProp")]
public class Public
{
    public int i { get; set; }

    public Public() {
        i = 2;
    }
   
    //[MyTest]
    private void TestMethod()
    {
        var j = 1;
    }
}