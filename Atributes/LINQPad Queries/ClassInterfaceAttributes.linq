<Query Kind="Program" />

void Main()
{
	Console.WriteLine($"{typeof(A).Name} - {typeof(A).Attributes}");
	
	var t=typeof(ITest).Attributes;

	Console.WriteLine(($"{typeof(ITest).Name} - {t}"));

	Console.WriteLine($"{typeof(Public).Name} - {typeof(Public).Attributes}");

	Console.WriteLine($"{typeof(Struct).Name} - {typeof(Struct).Attributes}");

	Console.WriteLine($"{typeof(Static).Name} - {typeof(Static).Attributes}");
	
	var test=new Public();
	test.TestMethod();
}

[Serializable]
class A
{
	private int C { get; set; }

	void SomeMethod()
	{
		Static.Method1();
	}
}

interface ITest
{
	static int i;
	int dosometh();

}

public class Public
{
	public int i { get; set; }

	public Public()
	{
		i = 2;
	}

	[MyTest]
	[Conditional("SomeCondition")]
	public void TestMethod()
	{
		var j = 1;
	}
}

struct Struct
{
	public byte a; // 1 byte
	public int b; // 4 bytes
	public short c; // 2 bytes
	public byte d; // 1 byte
}

public struct struct1
{
	public byte a; // 1 byte
	public int b; // 4 bytes
	public short c; // 2 bytes
	public byte d; // 1 byte
}

static class Static
{
	public static int i;

	public static void Method1() { i++; }
}


[AttributeUsage(AttributeTargets.All)]
public class MyTestAttribute : System.Attribute
{
	public String Name { get; set; }
	public MyTestAttribute(string testAttribute = "")
	{
		Name = testAttribute;
	}
	
	public String AditionalProperty { get; set; }
}
