<Query Kind="Program" />

void Main()
{
	var odd=Numbers.first|Numbers.second;   //0b0101
	var even=Numbers.second|Numbers.forth;  //0b1010
	odd.HasFlag(Numbers.first).Dump();
	odd.HasFlag((Numbers) 0).Dump(); 
	odd.HasFlag(even).Dump();
}

// You can define other methods, fields, classes and namespaces here

enum Numbers:short{
	first  =  0b0001,
	second =  0b0010,
	third  =  0b0100,
	forth  =  0b1000
}