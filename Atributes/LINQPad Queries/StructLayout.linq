<Query Kind="Program">
  <Namespace>System.Runtime.InteropServices</Namespace>
</Query>

using System.Runtime.InteropServices;

void Main()
{
	Console.WriteLine($"Size of Struct - {Marshal.SizeOf(new Struct())} byte");

	Console.WriteLine($"Size of struct1 -  {Marshal.SizeOf(new struct1())}  byte");
}

//[StructLayout(LayoutKind.Explicit)]
[StructLayout(LayoutKind.Sequential, Pack = 2)]
//[StructLayout(LayoutKind.Auto)]
//[StructLayout(LayoutKind.Explicit)]
struct Struct
{
	public byte a; // 1 byte
	public int b; // 4 bytes
	public short c; // 2 bytes
	public byte d; // 1 byte
}

public struct struct1
{
	public byte a; // 1 byte
	public int b; // 4 bytes
	public short c; // 2 bytes
	public byte d; // 1 byte
}